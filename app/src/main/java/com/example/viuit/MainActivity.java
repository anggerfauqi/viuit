package com.example.viuit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static boolean islogin = false;
    public static String username;

    TextView tes;

    NestedScrollView scroller;
    String sort[] = {"Descending", "Ascending"};

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    SwipeRefreshLayout refreshLayout;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);

        Button btn_logout = findViewById(R.id.btn_logout);

        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        isLoggedin();

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefManager.getInstance(getApplicationContext()).logout();
            }
        });
    }

    private void isLoggedin() {
        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            sendRequest();
        }
        else{
            Intent  intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void sendRequest() {
        //initialize progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        String url = "https://devel.bebasbayar.com/web/test_programmer.php";

        //  Initialize a new JsonArrayRequest instance
        java.util.List<Data> List = new ArrayList<>();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    progressDialog.dismiss();
                                    Log.d("Response", response.toString());

                                    Iterator<String> iter = response.keys(); //This should be the iterator you want.
                                    while(iter.hasNext()){

                                        String key = iter.next();
                                        Log.d("key",key);

                                        // fetch the data array
                                        JSONArray jsonArray = response.getJSONArray(key);

                                        // Extract data from json and store into ArrayList
                                        try {
                                            jsonArray = response.getJSONArray(key);
                                            for(int i=0; i<jsonArray.length(); i++){
                                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                                String date = key;
                                                String label = jsonObject.getString("label");
                                                String nb_visits = jsonObject.getString("nb_visits");
                                                String nb_hits = jsonObject.getString("nb_hits");
                                                String sum_time_spent = jsonObject.getString("sum_time_spent");

                                                String nb_hits_with_time_generation = jsonObject.getString("nb_hits_with_time_generation");
                                                String min_time_generation = jsonObject.getString("min_time_generation");
                                                String max_time_generation = jsonObject.getString("max_time_generation");
                                                String sum_bandwidth = jsonObject.getString("sum_bandwidth");
                                                String nb_hits_with_bandwidth = jsonObject.getString("nb_hits_with_bandwidth");

                                                String min_bandwidth = jsonObject.getString("min_bandwidth");
                                                String max_bandwidth = jsonObject.getString("max_bandwidth");
                                                String sum_daily_nb_uniq_visitors = jsonObject.getString("sum_daily_nb_uniq_visitors");
                                                String entry_nb_visits = jsonObject.getString("entry_nb_visits");
                                                String entry_nb_actions = jsonObject.getString("entry_nb_actions");

                                                String entry_sum_visit_length = jsonObject.getString("entry_sum_visit_length");
                                                String entry_bounce_count = jsonObject.getString("entry_bounce_count");
                                                String exit_nb_visits = jsonObject.getString("exit_nb_visits");
                                                String sum_daily_entry_nb_uniq_visitors = jsonObject.getString("sum_daily_entry_nb_uniq_visitors");
                                                String sum_daily_exit_nb_uniq_visitors = jsonObject.getString("sum_daily_exit_nb_uniq_visitors");

                                                String avg_bandwidth = jsonObject.getString("avg_bandwidth");
                                                String avg_time_on_page = jsonObject.getString("avg_time_on_page");
                                                String bounce_rate = jsonObject.getString("bounce_rate");
                                                String exit_rate = jsonObject.getString("exit_rate");
                                                String avg_time_generation = jsonObject.getString("avg_time_generation");

                                                Log.d("date",key);
                                                Log.d("label",jsonObject.getString("label"));
                                                Log.d("nb_visits",jsonObject.getString("nb_visits"));

                                                List.add(new Data(date, label, nb_visits, nb_hits, sum_time_spent, nb_hits_with_time_generation,
                                                        min_time_generation, max_time_generation, sum_bandwidth, nb_hits_with_bandwidth, min_bandwidth,
                                                        max_bandwidth, sum_daily_nb_uniq_visitors, entry_nb_visits, entry_nb_actions, entry_sum_visit_length,
                                                        entry_bounce_count, exit_nb_visits, sum_daily_entry_nb_uniq_visitors, sum_daily_exit_nb_uniq_visitors,
                                                        avg_bandwidth, avg_time_on_page, bounce_rate, exit_rate, avg_time_generation));

                                                adapter = new DataAdapter(getApplicationContext(), List);
                                                recyclerView.setAdapter(adapter);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

//                                    adapter = new DataAdapter(getApplicationContext(), List);
//                                    recyclerView.setAdapter(adapter);

                                } catch (JSONException jsonException) {
                                    jsonException.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i("Error Response",volleyError.toString());
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet! Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet! Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet! Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection Timeout! Please check your internet connection.";
                        }
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getApplicationContext(), "Refresh", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(false);
                sendRequest();
            }
        }, 1000);
    }
}