package com.example.viuit;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private Context context;
    private List<Data> data;

    public DataAdapter(Context context, List list) {
        this.context = context;
        this.data = list;
    }

    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_data, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(data.get(position));

        Data dt = data.get(position);

        holder.date.setText(": "+dt.getDate());
        holder.label.setText(": "+dt.getLabel());
        holder.nb_visits.setText(": "+dt.getNb_visits());

        holder.tvs1.setText(dt.getDate());
        holder.tvs2.setText(dt.getLabel());
        holder.tvs3.setText(dt.getNb_visits());
        holder.tvs4.setText(dt.getNb_hits());
        holder.tvs5.setText(dt.getSum_time_spent());

        holder.tvs6.setText(dt.getNb_hits_with_time_generation());
        holder.tvs7.setText(dt.getMin_time_generation());
        holder.tvs8.setText(dt.getMax_time_generation());
        holder.tvs9.setText(dt.getSum_bandwidth());
        holder.tvs10.setText(dt.getNb_hits_with_bandwidth());

        holder.tvs11.setText(dt.getMin_bandwidth());
        holder.tvs12.setText(dt.getMax_bandwidth());
        holder.tvs13.setText(dt.getSum_daily_nb_uniq_visitors());
        holder.tvs14.setText(dt.getEntry_nb_visits());
        holder.tvs15.setText(dt.getEntry_nb_actions());

        holder.tvs16.setText(dt.getEntry_sum_visit_length());
        holder.tvs17.setText(dt.getEntry_bounce_count());
        holder.tvs18.setText(dt.getExit_nb_visits());
        holder.tvs19.setText(dt.getSum_daily_entry_nb_uniq_visitors());
        holder.tvs20.setText(dt.getSum_daily_exit_nb_uniq_visitors());

        holder.tvs21.setText(dt.getAvg_bandwidth());
        holder.tvs22.setText(dt.getAvg_time_on_page());
        holder.tvs23.setText(dt.getBounce_rate());
        holder.tvs24.setText(dt.getExit_rate());
        holder.tvs25.setText(dt.getAvg_time_generation());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date, label, nb_visits, tvs1, tvs2, tvs3, tvs4, tvs5, tvs6, tvs7, tvs8, tvs9, tvs10,
                tvs11, tvs12, tvs13, tvs14, tvs15, tvs16, tvs17, tvs18, tvs19, tvs20,
                tvs21, tvs22, tvs23, tvs24, tvs25
                ;
        public Button show_detail;

        public ViewHolder(View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.tv_date);
            label = itemView.findViewById(R.id.tv_label);
            nb_visits = itemView.findViewById(R.id.tv_nbvisits);

            show_detail = itemView.findViewById(R.id.btn_show);

            tvs1 = itemView.findViewById(R.id.tvs1);
            tvs2 = itemView.findViewById(R.id.tvs2);
            tvs3 = itemView.findViewById(R.id.tvs3);
            tvs4 = itemView.findViewById(R.id.tvs4);
            tvs5 = itemView.findViewById(R.id.tvs5);
            tvs6 = itemView.findViewById(R.id.tvs6);
            tvs7 = itemView.findViewById(R.id.tvs7);
            tvs8 = itemView.findViewById(R.id.tvs8);
            tvs9 = itemView.findViewById(R.id.tvs9);
            tvs10 = itemView.findViewById(R.id.tvs10);
            tvs11 = itemView.findViewById(R.id.tvs11);
            tvs12 = itemView.findViewById(R.id.tvs12);
            tvs13 = itemView.findViewById(R.id.tvs13);
            tvs14 = itemView.findViewById(R.id.tvs14);
            tvs15 = itemView.findViewById(R.id.tvs15);
            tvs16 = itemView.findViewById(R.id.tvs16);
            tvs17 = itemView.findViewById(R.id.tvs17);
            tvs18 = itemView.findViewById(R.id.tvs18);
            tvs19 = itemView.findViewById(R.id.tvs19);
            tvs20 = itemView.findViewById(R.id.tvs20);
            tvs21 = itemView.findViewById(R.id.tvs21);
            tvs22 = itemView.findViewById(R.id.tvs22);
            tvs23 = itemView.findViewById(R.id.tvs23);
            tvs24 = itemView.findViewById(R.id.tvs24);
            tvs25 = itemView.findViewById(R.id.tvs25);


            show_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getRootView().getContext());
                    View dialog= LayoutInflater.from(view.getRootView().getContext()).inflate(R.layout.dialog_data,null);

                    TextView tv1 = (TextView) dialog.findViewById(R.id.tv1);
                    TextView tv2 = (TextView) dialog.findViewById(R.id.tv2);
                    TextView tv3 = (TextView) dialog.findViewById(R.id.tv3);
                    TextView tv4 = (TextView) dialog.findViewById(R.id.tv4);
                    TextView tv5 = (TextView) dialog.findViewById(R.id.tv5);
                    TextView tv6 = (TextView) dialog.findViewById(R.id.tv6);
                    TextView tv7 = (TextView) dialog.findViewById(R.id.tv7);
                    TextView tv8 = (TextView) dialog.findViewById(R.id.tv8);
                    TextView tv9 = (TextView) dialog.findViewById(R.id.tv9);
                    TextView tv10 = (TextView) dialog.findViewById(R.id.tv10);
                    TextView tv11 = (TextView) dialog.findViewById(R.id.tv11);
                    TextView tv12 = (TextView) dialog.findViewById(R.id.tv12);
                    TextView tv13 = (TextView) dialog.findViewById(R.id.tv13);
                    TextView tv14 = (TextView) dialog.findViewById(R.id.tv14);
                    TextView tv15 = (TextView) dialog.findViewById(R.id.tv15);
                    TextView tv16 = (TextView) dialog.findViewById(R.id.tv16);
                    TextView tv17 = (TextView) dialog.findViewById(R.id.tv17);
                    TextView tv18 = (TextView) dialog.findViewById(R.id.tv18);
                    TextView tv19 = (TextView) dialog.findViewById(R.id.tv19);
                    TextView tv20 = (TextView) dialog.findViewById(R.id.tv20);
                    TextView tv21 = (TextView) dialog.findViewById(R.id.tv21);
                    TextView tv22 = (TextView) dialog.findViewById(R.id.tv22);
                    TextView tv23 = (TextView) dialog.findViewById(R.id.tv23);
                    TextView tv24 = (TextView) dialog.findViewById(R.id.tv24);
                    TextView tv25 = (TextView) dialog.findViewById(R.id.tv25);

                    tv1.setText(": "+tvs1.getText());
                    tv2.setText(": "+tvs2.getText());
                    tv3.setText(": "+tvs3.getText());
                    tv4.setText(": "+tvs4.getText());
                    tv5.setText(": "+tvs5.getText());
                    tv6.setText(": "+tvs6.getText());
                    tv7.setText(": "+tvs7.getText());
                    tv8.setText(": "+tvs8.getText());
                    tv9.setText(": "+tvs9.getText());
                    tv10.setText(": "+tvs10.getText());
                    tv11.setText(": "+tvs11.getText());
                    tv12.setText(": "+tvs12.getText());
                    tv13.setText(": "+tvs13.getText());
                    tv14.setText(": "+tvs14.getText());
                    tv15.setText(": "+tvs15.getText());
                    tv16.setText(": "+tvs16.getText());
                    tv17.setText(": "+tvs17.getText());
                    tv18.setText(": "+tvs18.getText());
                    tv19.setText(": "+tvs19.getText());
                    tv20.setText(": "+tvs20.getText());
                    tv21.setText(": "+tvs21.getText());
                    tv22.setText(": "+tvs22.getText());
                    tv23.setText(": "+tvs23.getText());
                    tv24.setText(": "+tvs24.getText());
                    tv25.setText(": "+tvs25.getText());

                    builder.setView(dialog);
                    builder.setCancelable(true);
                    builder.show();

                }
            });
        }
    }
}
