package com.example.viuit;

public class Data {
    String date, label, nb_visits, nb_hits, sum_time_spent, nb_hits_with_time_generation,
            min_time_generation, max_time_generation, sum_bandwidth, nb_hits_with_bandwidth, min_bandwidth,
            max_bandwidth, sum_daily_nb_uniq_visitors, entry_nb_visits, entry_nb_actions, entry_sum_visit_length,
            entry_bounce_count, exit_nb_visits, sum_daily_entry_nb_uniq_visitors, sum_daily_exit_nb_uniq_visitors,
            avg_bandwidth, avg_time_on_page, bounce_rate, exit_rate, avg_time_generation;

    public Data() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNb_visits() {
        return nb_visits;
    }

    public void setNb_visits(String nb_visits) {
        this.nb_visits = nb_visits;
    }

    public String getNb_hits() {
        return nb_hits;
    }

    public void setNb_hits(String nb_hits) {
        this.nb_hits = nb_hits;
    }

    public String getSum_time_spent() {
        return sum_time_spent;
    }

    public void setSum_time_spent(String sum_time_spent) {
        this.sum_time_spent = sum_time_spent;
    }

    public String getNb_hits_with_time_generation() {
        return nb_hits_with_time_generation;
    }

    public void setNb_hits_with_time_generation(String nb_hits_with_time_generation) {
        this.nb_hits_with_time_generation = nb_hits_with_time_generation;
    }

    public String getMin_time_generation() {
        return min_time_generation;
    }

    public void setMin_time_generation(String min_time_generation) {
        this.min_time_generation = min_time_generation;
    }

    public String getMax_time_generation() {
        return max_time_generation;
    }

    public void setMax_time_generation(String max_time_generation) {
        this.max_time_generation = max_time_generation;
    }

    public String getSum_bandwidth() {
        return sum_bandwidth;
    }

    public void setSum_bandwidth(String sum_bandwidth) {
        this.sum_bandwidth = sum_bandwidth;
    }

    public String getNb_hits_with_bandwidth() {
        return nb_hits_with_bandwidth;
    }

    public void setNb_hits_with_bandwidth(String nb_hits_with_bandwidth) {
        this.nb_hits_with_bandwidth = nb_hits_with_bandwidth;
    }

    public String getMin_bandwidth() {
        return min_bandwidth;
    }

    public void setMin_bandwidth(String min_bandwidth) {
        this.min_bandwidth = min_bandwidth;
    }

    public String getMax_bandwidth() {
        return max_bandwidth;
    }

    public void setMax_bandwidth(String max_bandwidth) {
        this.max_bandwidth = max_bandwidth;
    }

    public String getSum_daily_nb_uniq_visitors() {
        return sum_daily_nb_uniq_visitors;
    }

    public void setSum_daily_nb_uniq_visitors(String sum_daily_nb_uniq_visitors) {
        this.sum_daily_nb_uniq_visitors = sum_daily_nb_uniq_visitors;
    }

    public String getEntry_nb_visits() {
        return entry_nb_visits;
    }

    public void setEntry_nb_visits(String entry_nb_visits) {
        this.entry_nb_visits = entry_nb_visits;
    }

    public String getEntry_nb_actions() {
        return entry_nb_actions;
    }

    public void setEntry_nb_actions(String entry_nb_actions) {
        this.entry_nb_actions = entry_nb_actions;
    }

    public String getEntry_sum_visit_length() {
        return entry_sum_visit_length;
    }

    public void setEntry_sum_visit_length(String entry_sum_visit_length) {
        this.entry_sum_visit_length = entry_sum_visit_length;
    }

    public String getEntry_bounce_count() {
        return entry_bounce_count;
    }

    public void setEntry_bounce_count(String entry_bounce_count) {
        this.entry_bounce_count = entry_bounce_count;
    }

    public String getExit_nb_visits() {
        return exit_nb_visits;
    }

    public void setExit_nb_visits(String exit_nb_visits) {
        this.exit_nb_visits = exit_nb_visits;
    }

    public String getSum_daily_entry_nb_uniq_visitors() {
        return sum_daily_entry_nb_uniq_visitors;
    }

    public void setSum_daily_entry_nb_uniq_visitors(String sum_daily_entry_nb_uniq_visitors) {
        this.sum_daily_entry_nb_uniq_visitors = sum_daily_entry_nb_uniq_visitors;
    }

    public String getSum_daily_exit_nb_uniq_visitors() {
        return sum_daily_exit_nb_uniq_visitors;
    }

    public void setSum_daily_exit_nb_uniq_visitors(String sum_daily_exit_nb_uniq_visitors) {
        this.sum_daily_exit_nb_uniq_visitors = sum_daily_exit_nb_uniq_visitors;
    }

    public String getAvg_bandwidth() {
        return avg_bandwidth;
    }

    public void setAvg_bandwidth(String avg_bandwidth) {
        this.avg_bandwidth = avg_bandwidth;
    }

    public String getAvg_time_on_page() {
        return avg_time_on_page;
    }

    public void setAvg_time_on_page(String avg_time_on_page) {
        this.avg_time_on_page = avg_time_on_page;
    }

    public String getBounce_rate() {
        return bounce_rate;
    }

    public void setBounce_rate(String bounce_rate) {
        this.bounce_rate = bounce_rate;
    }

    public String getExit_rate() {
        return exit_rate;
    }

    public void setExit_rate(String exit_rate) {
        this.exit_rate = exit_rate;
    }

    public String getAvg_time_generation() {
        return avg_time_generation;
    }

    public void setAvg_time_generation(String avg_time_generation) {
        this.avg_time_generation = avg_time_generation;
    }

    public Data(String date, String label, String nb_visits, String nb_hits, String sum_time_spent, String nb_hits_with_time_generation, String min_time_generation, String max_time_generation,
                String sum_bandwidth, String nb_hits_with_bandwidth, String min_bandwidth, String max_bandwidth, String sum_daily_nb_uniq_visitors, String entry_nb_visits, String entry_nb_actions, String entry_sum_visit_length, String entry_bounce_count, String exit_nb_visits, String sum_daily_entry_nb_uniq_visitors, String sum_daily_exit_nb_uniq_visitors, String avg_bandwidth, String avg_time_on_page, String bounce_rate, String exit_rate, String avg_time_generation) {
        this.date = date;
        this.label = label;
        this.nb_visits = nb_visits;
        this.nb_hits = nb_hits;
        this.sum_time_spent = sum_time_spent;
        this.nb_hits_with_time_generation = nb_hits_with_time_generation;
        this.min_time_generation = min_time_generation;
        this.max_time_generation = max_time_generation;
        this.sum_bandwidth = sum_bandwidth;
        this.nb_hits_with_bandwidth = nb_hits_with_bandwidth;
        this.min_bandwidth = min_bandwidth;
        this.max_bandwidth = max_bandwidth;
        this.sum_daily_nb_uniq_visitors = sum_daily_nb_uniq_visitors;
        this.entry_nb_visits = entry_nb_visits;
        this.entry_nb_actions = entry_nb_actions;
        this.entry_sum_visit_length = entry_sum_visit_length;
        this.entry_bounce_count = entry_bounce_count;
        this.exit_nb_visits = exit_nb_visits;
        this.sum_daily_entry_nb_uniq_visitors = sum_daily_entry_nb_uniq_visitors;
        this.sum_daily_exit_nb_uniq_visitors = sum_daily_exit_nb_uniq_visitors;
        this.avg_bandwidth = avg_bandwidth;
        this.avg_time_on_page = avg_time_on_page;
        this.bounce_rate = bounce_rate;
        this.exit_rate = exit_rate;
        this.avg_time_generation = avg_time_generation;
    }

}
