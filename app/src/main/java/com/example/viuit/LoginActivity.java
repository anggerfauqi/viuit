package com.example.viuit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText username, password;
    Button b_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        b_login = findViewById(R.id.btn_login);

        b_login.setOnClickListener(view -> sendRequest());

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }


    }

    private void sendRequest() {
        //initialize progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        //validating inputs
        if (TextUtils.isEmpty(username.getText().toString())) {
            username.setError("Please enter your username");
            username.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("Please enter your password");
            password.requestFocus();
            return;
        }

        String url = "https://devel.bebasbayar.com/web/test_programmer.php";

        Map<String, String> postParam= new HashMap<String, String>();
        postParam.put("user", username.getText().toString());
        postParam.put("password", password.getText().toString());

        //if everything is fine
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        //converting response to json object
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(String.valueOf(response));
                            String rd = obj.getString("rd");
                            Toast.makeText(getApplicationContext(), rd, Toast.LENGTH_SHORT).show();

                            SharedPrefManager.getInstance(getApplicationContext()).userLogin(username.getText().toString());

                            //starting the profile activity
                            if(rd.equals("Sukses")){
                                finish();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), rd, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String TAG = "Message";
                        VolleyLog.d(TAG, error.getMessage());
                        progressDialog.dismiss();
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjReq);
    }

}